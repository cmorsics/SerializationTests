﻿namespace SerializationTests.TestClasses
{
    public class TestValueTypeProperties
    {
        public bool BoolTest
        {
            get;
            set;
        }

        public int IntTest
        {
            get;
            set;
        }

        public decimal DecimalTest
        {
            get;
            set;
        }

        public TestValueTypeProperties()
        { }
    }
}
