﻿namespace SerializationTests.TestClasses
{

    public class TestReferenceTypeProperties
    {
        public string StringTest
        {
            get;
            set;
        }

        public TestValueTypeProperties ClassTest
        {
            get;
            set;
        }

        public TestReferenceTypeProperties()
        { }
    }
}
