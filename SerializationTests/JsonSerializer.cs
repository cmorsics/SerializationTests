﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace SerializationTests
{
    public class JsonSerializer : AbstractSerializer
    {
        public override void RunTest()
        {
            TestSerializationForNamespace("SerializationTests.TestClasses");
        }

        protected override void TestSerializer(Type dataType, Object instance)
        {
            SetDefaultValueForEnums(dataType, instance);

            var serialize = Serialize(instance);

            var deserialize = typeof(JsonSerializer)
                .GetMethod("Deserialize")
                .MakeGenericMethod(dataType)
	            .Invoke(this, new object[] { serialize});

            Assert.IsNotNull(serialize);
            Assert.IsNotNull(deserialize);
        }

        private readonly JsonSerializerSettings _jsonSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.None
        };

        protected override string Serialize(object instance)
        {
            return JsonConvert.SerializeObject(instance, _jsonSettings);
        }

        public override T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _jsonSettings);
        }
    }
}
