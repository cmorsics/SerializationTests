﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SerializationTests
{
    public abstract class AbstractSerializer
    {
        public abstract void RunTest();
        protected abstract string Serialize(object instance);
        public abstract T Deserialize<T>(string json);
        protected abstract void TestSerializer(Type dataType, Object instance);

        protected void TestSerializationForNamespace(string strNamespace)
        {
            var failures = new StringBuilder();

            var dataTypeObjects = GetAssemblyTypesForNamespace(strNamespace);

            foreach (var dataTypeObject in dataTypeObjects)
            {
                try
                {
                    TestSerializer(dataTypeObject, CreateInstance(dataTypeObject));
                }
                catch (Exception e)
                {
                    failures.AppendFormat("Class type {0} failed to serialize and deserialize. {1}",
                        dataTypeObject,
                        e.Message);
                }
            }

            if (failures.Length > 0)
                Assert.Fail(failures.ToString());
        }

        protected static object CreateInstance(Type objectType)
        {
            object instance;

            if (!objectType.ContainsGenericParameters)
                instance = Activator.CreateInstance(objectType);
            else
            {
                var genericType = objectType.MakeGenericType(typeof(int));
                instance = Activator.CreateInstance(genericType);
            }

            CreateEmptySubObjects(instance);

            return instance;
        }

        protected static void CreateEmptySubObjects(object instance)
        {
            var props = instance.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var prop in props
                .Where(prop => prop.CanWrite && prop.PropertyType.GetConstructor(Type.EmptyTypes) != null))
            {
                prop.SetValue(instance, Activator.CreateInstance(prop.PropertyType), null);
            }
        }

        protected static void SetDefaultValueForEnums(Type dataType, object instance)
        {
            foreach (var propertyInfo in dataType.GetProperties())
            {
                if (propertyInfo.DeclaringType == null)
                    continue;

                if (!propertyInfo.PropertyType.IsEnum)
                    continue;

                var enumValue = propertyInfo
                    .PropertyType
                    .GetFields(BindingFlags.Static | BindingFlags.Public)
                    .First()
                    .GetValue(null);

                propertyInfo.SetValue(instance, enumValue, null);
            }
        }

        protected static IEnumerable<Type> GetAssemblyTypesForNamespace(string searchNamespace)
        {
            var dataTypeObjects = new List<Type>();

            try
            {
                foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    dataTypeObjects.AddRange(
                        a.GetTypes()
                            .Where(x => x.Namespace != null && x.Namespace.Contains(searchNamespace)
                                        && !x.Namespace.Contains("Temp")
                                        && !x.Namespace.Contains("ADP.DTO.Payments")
                                        && x.IsAbstract == false && x.IsEnum == false && x.IsClass
                                        && Attribute.GetCustomAttribute(x, typeof(GeneratedCodeAttribute)) == null)
                            .ToList());
                }
            }
            catch (Exception e)
            {
                Assert.Inconclusive("Error getting assemblies: {0}", e.Message);
            }

            return dataTypeObjects;
        }
    }
}
